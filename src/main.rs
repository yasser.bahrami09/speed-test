use std::{fs, io, thread};
use std::env;
use std::io::{Read, Write};
use std::net::{TcpStream, Shutdown};
use std::str::FromStr;
use std::time::{Duration, Instant};

use rand::random;
use serde::{Deserialize, Serialize};

#[derive(Debug, Copy, Clone)]
pub enum ProtocolType {
    Tcp,
    Udp,
    Rds,
}

impl FromStr for ProtocolType {
    type Err = ();

    fn from_str(input: &str) -> Result<ProtocolType, Self::Err> {
        match input {
            "tcp" => Ok(ProtocolType::Tcp),
            "udp" => Ok(ProtocolType::Udp),
            "rds" => Ok(ProtocolType::Rds),
            _ => Err(()),
        }
    }
}

struct Protocol {
    protocol_type: ProtocolType
}

trait PerformanceMetrics {
    fn new(protocol_type: ProtocolType) -> Self;
    fn speed_test(&self, ip: String, port: i32, timeout: u64, upload_time: u64, download_time: u64) -> bool;
}

impl PerformanceMetrics for Protocol {
    fn new(protocol_type: ProtocolType) -> Protocol {
        Protocol { protocol_type: protocol_type }
    }

    fn speed_test(&self, ip: String, port: i32, timeout: u64, upload_time: u64, download_time: u64) -> bool {
        match self.protocol_type {
            ProtocolType::Tcp => {
                if timeout <= 0 {
                    println!("invalid timeout value {}", timeout);
                    return false;
                } else if port > 65535 || port < 0 {
                    println!("invalid port value {}", port);
                    return false;
                } else if upload_time <= 0 {
                    println!("invalid upload time value {}", upload_time);
                    return false;
                } else if download_time <= 0 {
                    println!("invalid download time value {}", download_time);
                    return false;
                }
                return match TcpStream::connect(format!("{}:{}", ip, port)) {
                    Ok(mut stream) => {
                        println!("Speed Test started!");
                        let random_bytes: Vec<u8> = (0..8192).map(|_| { random::<u8>() }).collect();
                        let upload_finish_command = b"upload test finish";
                        let download_finish_command = b"download test finish";
                        let mut upload_bytes: i64 = 0;
                        let mut download_bytes: i64 = 0;
                        let upload_elapsed = Instant::now();
                        let read_timeout: Option<Duration> = Option::from(Duration::from_secs(timeout));
                        let write_timeout: Option<Duration> = Option::from(Duration::from_secs(timeout));
                        let _read_return: io::Result<()> = stream.set_read_timeout(read_timeout);
                        let _write_return: io::Result<()> = stream.set_write_timeout(write_timeout);
                        let upload_duration = Duration::from_secs(upload_time);
                        let download_duration = Duration::from_secs(upload_time + 5);
                        println!("Uploading...");
                        loop {
                            let write_res = stream.write(random_bytes.as_slice());
                            let _flush_res = stream.flush();
                            match write_res {
                                Ok(n) => {upload_bytes = upload_bytes + n as i64},
                                _ => {}
                            }
                            if upload_elapsed.elapsed() >= upload_duration {
                                break;
                            }
                            thread::sleep(Duration::from_millis(1));
                        }
                        let _write_res = stream.write(upload_finish_command);
                        let _flush_res = stream.flush();
                        let up_time = upload_elapsed.elapsed().as_secs();
                        let mut buffer = [0; 8192];
                        let start = Instant::now();
                        println!("Downloading...");
                        loop {
                            let result = stream.read(&mut buffer);
                            match result {
                                Ok(n) => {download_bytes = download_bytes + n as i64},
                                _ => {}
                            }
                            if buffer.starts_with(download_finish_command) {
                                break;
                            }
                            if start.elapsed() >= download_duration {
                                break;
                            }
                            thread::sleep(Duration::from_millis(1));
                        }
                        println!("[Download] {} MB/s (for {} seconds)", download_bytes as f32 / (1024.0 * 1024.0), start.elapsed().as_secs());
                        println!("[Upload] {} MB/s (for {} seconds)", upload_bytes as f32 / (1024.0 * 1024.0), up_time);
                        let _result = stream.shutdown(Shutdown::Both);
                        true
                    }
                    Err(e) => {
                        println!("failed to connect {:?}", e);
                        false
                    }
                };
            }
            _ => {
                println!("speed test for this protocol is not implemented");
                false
            }
        }
    }
}

fn main() {
    let args: Vec<String> = env::args().collect();
    if args.len() <= 1 {
        panic!("config file missing!");
    }
    let config_file_name = &args[1];
    let config_str: String = fs::read_to_string(config_file_name).unwrap();
    let config: Config = serde_json::from_str(&config_str).unwrap();
    let protocol: Protocol = PerformanceMetrics::new(ProtocolType::from_str(config.protocol.as_ref()).unwrap());
    protocol.speed_test(config.ip, config.port, config.timeout, config.upload_time, config.download_time);
}


#[derive(Clone, Debug, Serialize, Deserialize)]
struct Config {
    ip: String,
    port: i32,
    protocol: String,
    timeout: u64,
    download_time: u64,
    upload_time: u64,
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_speed_test_udp() {
        let protocol: Protocol = PerformanceMetrics::new(ProtocolType::Udp);
        assert_eq!(protocol.speed_test("127.0.0.1".to_string(), 8080, 1000, 30000, 30000), false);
    }

    #[test]
    fn test_speed_test_invalid_timeout() {
        let protocol: Protocol = PerformanceMetrics::new(ProtocolType::Tcp);
        assert_eq!(protocol.speed_test("127.0.0.1".to_string(), 8080, 0, 30000, 30000), false);
    }

    #[test]
    fn test_speed_test_invalid_port() {
        let protocol: Protocol = PerformanceMetrics::new(ProtocolType::Tcp);
        assert_eq!(protocol.speed_test("127.0.0.1".to_string(), 80800, 1000, 30000, 30000), false);
    }

    #[test]
    fn test_speed_test_invalid_upload_time() {
        let protocol: Protocol = PerformanceMetrics::new(ProtocolType::Tcp);
        assert_eq!(protocol.speed_test("127.0.0.1".to_string(), 8080, 1000, 0, 30000), false);
    }

    #[test]
    fn test_speed_test_download_time() {
        let protocol: Protocol = PerformanceMetrics::new(ProtocolType::Tcp);
        assert_eq!(protocol.speed_test("127.0.0.1".to_string(), 8080, 1000, 30000, 0), false);
    }
    #[test]
    fn test_speed_test_not_connected() {
        let protocol: Protocol = PerformanceMetrics::new(ProtocolType::Tcp);
        assert_eq!(protocol.speed_test("127.0.0.1".to_string(), 8080, 1000, 30000, 30000), false);
    }
}